import { rendererService as renderer } from 'core/services';

export function App() {
    const self = this;

    self.init = function() {
      renderer.process(document.body);
    }
}
