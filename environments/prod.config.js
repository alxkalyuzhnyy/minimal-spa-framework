module.exports = {
    build: {
        compress: true,
        devtool: false
    },
    runtime: {
        name: 'prod',
        api: '/api/'
    }
};
